//
//  OrdersViewController.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "OrdersViewController.h"
#import "OrderTableViewCell.h"
#import "CCMWSUtilities+Orders.h"
#import "OrderDetailViewController.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>
#import "CurrentContext.h"

@interface OrdersViewController ()
@property (nonatomic, retain) NSMutableArray *ordersDataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLabelHeightConstraint;
@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Commandes";
    
    
    [self getOrders];
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor blueColor];
    [self.refreshControl addTarget:self
                            action:@selector(getOrders)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _topLabelHeightConstraint.constant = 0;
    [self.view layoutIfNeeded];
}

- (void)getOrders{
    [CCMWSUtilities getAllOrders:^(BOOL succeed, NSMutableArray<CCMOrder *> *orders, BOOL timedOut) {
        if (succeed) {
            _ordersDataSource = orders;
            [_tableView reloadData];
            [_refreshControl endRefreshing];
        }else if (timedOut){
            if ([CurrentContext sharedInstance].circuitBreakerOpened) {
                [self timedOut];
            }else{
                NSLog(@"On reload les orders : %ld", (long)[CurrentContext sharedInstance].countOfFailedCalls);
                [self getOrders];
            }
        }
    }];
}

- (void)timedOut{
    _topLabelHeightConstraint.constant = 40;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    [_ordersDataSource removeAllObjects];
    for (CCMOrder *order in [CCMOrder allObjects]) {
        [_ordersDataSource addObject:order];
    }
    [_tableView reloadData];
        
//        [UIAlertController showWithTitle:@"Timeout" message:@"La requête a mit trop de temps à être traitée" okButtonTitle:@"Réessayer ?" onOKAction:^(UIAlertController *alertController) {
//            [self getOrders];
//        }];
}
//--------------------------------------------------------------------------------------------------
#pragma mark - UITableView Delegates
//--------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _ordersDataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"OrderTableViewCellID";
    OrderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[OrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell fillWithOrder:[_ordersDataSource objectAtIndex:indexPath.row]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    OrderDetailViewController *detailVC = [UIStoryboard getViewControllerWithIdentifier:@"OrderDetailVC" inStoryboardWithName:@"Main"];
    detailVC.order = [_ordersDataSource objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
