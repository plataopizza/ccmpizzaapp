//
//  OrderDetailViewController.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMOrder.h"

@interface OrderDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (weak, nonatomic) IBOutlet UILabel *orderPizzaName;
@property (weak, nonatomic) IBOutlet UILabel *orderPizzaPrice;
@property (nonatomic, retain) CCMOrder *order;
@end
