//
//  HomeViewController.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "HomeViewController.h"
#import "CCMWSUtilities.h"
#import "CCMWSUtilities+Pizzas.h"
#import "CCMWSUtilities+Orders.h"
#import "PizzaTableViewCell.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>
#import "OrdersViewController.h"
#import "OrderDetailViewController.h"
#import "StatusViewController.h"
#import "CurrentContext.h"
#import <TwitterKit/TwitterKit.h>

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *pizzasDatasource;
@property BOOL passingOrder;
@property (weak, nonatomic) IBOutlet UIButton *seeStatusButton;
- (IBAction)seeStatusAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
- (IBAction)twitterAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLabelHeightConstraint;
@property BOOL inTimeOut;
@end

@implementation HomeViewController

//--------------------------------------------------------------------------------------------------
#pragma mark - Life Cycle
//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Pizzas";
    _passingOrder = NO;
    _topLabelHeightConstraint.constant = 0;
    [self.view layoutIfNeeded];
    _inTimeOut = NO;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self checkShowedTimeout];
    _seeStatusButton.hidden = YES;
    _twitterButton.hidden = YES;
    UIBarButtonItem *orders=[[UIBarButtonItem alloc]initWithTitle:@"Admin" style:UIBarButtonItemStylePlain target:self action:@selector(seeOrders)];
    self.navigationItem.rightBarButtonItem=orders;
    [self loadData];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}


- (void)loadData{
    _pizzasDatasource = [NSMutableArray new];
    _topLabelHeightConstraint.constant = 0;
    [self.view layoutIfNeeded];
    
    [CCMWSUtilities getAllPizzas:^(BOOL succeed, NSMutableArray<CCMPizza *> *pizzas, BOOL timedOut) {
        if (succeed) {
            _seeStatusButton.hidden = YES;
            _twitterButton.hidden = NO;
            UIBarButtonItem *myOrder=[[UIBarButtonItem alloc]initWithTitle:@"Ma commande" style:UIBarButtonItemStylePlain target:self action:@selector(seeMyOrder)];
            self.navigationItem.leftBarButtonItem=myOrder;
            
            _pizzasDatasource = pizzas;
            [_tableView reloadData];
        }else if (timedOut){
            if ([CurrentContext sharedInstance].circuitBreakerOpened) {
                [self timedOut];
            }else{
                NSLog(@"On reload les pizzas : %ld", (long)[CurrentContext sharedInstance].countOfFailedCalls);
                [self loadData];
            }
        }
        
    }];
}


- (void)seeMyOrder{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Commande" message:@"Entrez le numéro de votre commande" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"N° de commande";
    }];
    
    [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alert.textFields[0];
        if ([textField.text isNumber]) {
            
            [CCMWSUtilities getOrderWithID:textField.text completion:^(BOOL succeed, CCMOrder *order, BOOL timedOut) {
                if (succeed) {
                    OrderDetailViewController *detailVC = [UIStoryboard getViewControllerWithIdentifier:@"OrderDetailVC" inStoryboardWithName:@"Main"];
                    detailVC.order = order;
                    [self.navigationController pushViewController:detailVC animated:YES];
                }else if(timedOut){
                    // Ne devrait pas arriver, vu que le bouton est caché quand on est timed out
                }else{
                    [UIAlertController showWithTitle:@"Erreur" message:@"Une erreur est survenue lors de la récupération de votre commande" okButtonTitle:@"OK" onOKAction:nil];
                }
            }];
        }else{
            [UIAlertController showWithTitle:@"Erreur" message:@"Votre numéro de commande n'est pas valide" okButtonTitle:@"OK" onOKAction:nil];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Annuler" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"Cancel pressed");
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)seeStatusAction:(id)sender {
    StatusViewController *statusVC = [UIStoryboard getViewControllerWithIdentifier:@"StatusVC" inStoryboardWithName:@"Main"];
    [self.navigationController pushViewController:statusVC animated:YES];
}

- (void)seeOrders{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Admin" message:@"Entrez le mot de passe administrateur pour accéder à la liste des commandes" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.secureTextEntry = YES;
        textField.text = @"admin";
    }];
    
    [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alert.textFields[0];
        if ([textField.text isEqualToString:@"admin"]) {
            OrdersViewController *ordersVC = [UIStoryboard getViewControllerWithIdentifier:@"OrdersVC" inStoryboardWithName:@"Main"];
            [self.navigationController pushViewController:ordersVC animated:YES];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Annuler" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"Cancel pressed");
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)timedOut{
    _inTimeOut = YES;
    _topLabelHeightConstraint.constant = 40;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    [_pizzasDatasource removeAllObjects];
    for (CCMPizza *pizza in [CCMPizza allObjects]) {
        [_pizzasDatasource addObject:pizza];
    }
    [_tableView reloadData];
    _seeStatusButton.hidden = NO;
    _twitterButton.hidden = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

//--------------------------------------------------------------------------------------------------
#pragma mark - UITableView Delegates
//--------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _pizzasDatasource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"PizzaCellID";
    PizzaTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PizzaTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell fillWithPizza:[_pizzasDatasource objectAtIndex:indexPath.row] timeOut:_inTimeOut];
    
    cell.didPressOrderButton = ^{
        _passingOrder = YES;
        [CCMWSUtilities postOrder:[_pizzasDatasource objectAtIndex:indexPath.row] completion:^(BOOL succeed, CCMOrder *order, BOOL timedOut) {
            _passingOrder = NO;
            if (succeed) {
              _passingOrder = NO;
              [UIAlertController showWithTitle:@"Commande passée" message:[NSString stringWithFormat:@"Votre commande numéro %ld a bien été passée, retenez le numéro de commande !", (long)order.orderID] okButtonTitle:@"OK" onOKAction:nil];
            }else if(timedOut){
             // Ne devrait pas arriver, en timed out on cache
            }else{
               [UIAlertController showWithTitle:@"Erreur" message:@"Une erreur est survenue lors du passage de votre commande" okButtonTitle:@"OK" onOKAction:nil];
            }
      }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (IBAction)twitterAction:(id)sender {
    [self showTimeline];
}

- (void)showTimeline {
    // Create an API client and data source to fetch Tweets for the timeline
    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
    TWTRUserTimelineDataSource *dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"plataopizza" APIClient:client];
    TWTRTimelineViewController *controller = [[TWTRTimelineViewController alloc] initWithDataSource:dataSource];
    // Create done button to dismiss the view controller
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissTimeline)];
    controller.navigationItem.leftBarButtonItem = button;
    // Create a navigation controller to hold the
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self showDetailViewController:navigationController sender:self];
}

- (void)dismissTimeline {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
