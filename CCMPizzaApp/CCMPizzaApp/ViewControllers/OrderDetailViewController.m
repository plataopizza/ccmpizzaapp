//
//  OrderDetailViewController.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "OrderDetailViewController.h"

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_order) {
        _orderNumber.text = [NSString stringWithFormat:@"Votre commande N° %ld", (long)_order.orderID];
        _orderStatus.text = [NSString stringWithFormat:@"Status : %@", _order.status];
        _orderPizzaName.text = [NSString stringWithFormat:@"Pizza : %@", _order.item.name];
        _orderPizzaPrice.text = [NSString stringWithFormat:@"Prix : %.02f", _order.item.price];
    }

}


@end
