//
//  StatusViewController.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "StatusViewController.h"
#import <MessageUI/MessageUI.h>
#import <TwitterKit/TwitterKit.h>

@interface StatusViewController ()<UIWebViewDelegate, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)phoneAction:(id)sender;
- (IBAction)mailAction:(id)sender;
- (IBAction)twitterAction:(id)sender;

@end

@implementation StatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Status";
    _webView.delegate = self;
    [self startLoading];
    
    UIBarButtonItem *orders=[[UIBarButtonItem alloc]initWithTitle:@"Recharger" style:UIBarButtonItemStylePlain target:self action:@selector(startLoading)];
    self.navigationItem.rightBarButtonItem=orders;
}

- (void)startLoading{
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plataopizzainformation.herokuapp.com"]]];

}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}

- (IBAction)phoneAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://0668411612"] options:@{} completionHandler:nil];
}

- (IBAction)mailAction:(id)sender {
    // Email Subject
    NSString *emailTitle = @"Ma commande Plata O Pizza";
    // Email Content
    NSString *messageBody = @"Veuillez décrire votre commande :";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"plataopizza@yopmail.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (IBAction)twitterAction:(id)sender {
    [self showTimeline];
}
- (void)showTimeline {
    // Create an API client and data source to fetch Tweets for the timeline
    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
    TWTRUserTimelineDataSource *dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"plataopizza" APIClient:client];
    TWTRTimelineViewController *controller = [[TWTRTimelineViewController alloc] initWithDataSource:dataSource];
    // Create done button to dismiss the view controller
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissTimeline)];
    controller.navigationItem.leftBarButtonItem = button;
    // Create a navigation controller to hold the
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self showDetailViewController:navigationController sender:self];
}

- (void)dismissTimeline {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
