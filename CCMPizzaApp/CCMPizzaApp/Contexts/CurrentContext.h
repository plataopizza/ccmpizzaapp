//
//  CurrentContext.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 19/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentContext : NSObject
@property BOOL circuitBreakerOpened;
@property (nonatomic) NSInteger countOfFailedCalls;

//Singleton vers une seule instance CurrentContext...
+ (CurrentContext *)sharedInstance;
//Initialisation du contexte...
- (void)initializeCurrentContext;
@end
