//
//  CurrentContext.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 19/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CurrentContext.h"
#import "CCMWSUtilities+Orders.h"
#import "CCMWSUtilities+Pizzas.h"
#import <Rollbar/Rollbar.h>

@implementation CurrentContext
//--------------------------------------------------------------------------------------------------
#pragma mark - Life cycle...
//--------------------------------------------------------------------------------------------------
//Singleton vers une seule instance CurrentContext...
+ (CurrentContext *)sharedInstance{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

//Dealloc...
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques...
//--------------------------------------------------------------------------------------------------
//Initialisation du shared instance...
- (void)initializeCurrentContext{
    
    _countOfFailedCalls = 0;
    [CCMWSUtilities getAllPizzas:^(BOOL succeed, NSMutableArray<CCMPizza *> *pizzas, BOOL timedOut) {
        if (succeed) {
            [CCMWSUtilities getAllOrders:^(BOOL succeed, NSMutableArray<CCMOrder *> *orders, BOOL timedOut) {
                
            }];
        }
    }];
    
    
}

-(void)setCountOfFailedCalls:(NSInteger)countOfFailedCalls{
    _countOfFailedCalls = countOfFailedCalls;
    if (_countOfFailedCalls == 4) {
        NSLog(@"FAILED TOO MANY TIMES");
        _circuitBreakerOpened = YES;
        
        // Log a critical, with some additional key-value data
        [Rollbar criticalWithMessage:@"Timeout API Pizza" data:@{@"countOfFailedCalls":[NSNumber numberWithInteger:_countOfFailedCalls]}];
        
        [self prepareTimer];
        
    }
}

- (void)prepareTimer{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(180 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self resetCounter];
    });
}

- (void)resetCounter{
    _countOfFailedCalls = 3;
    _circuitBreakerOpened = NO;

}
@end
