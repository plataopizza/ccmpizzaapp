//
//  OrderTableViewCell.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMOrder.h"

@interface OrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *countPizzasLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

-(void)fillWithOrder:(CCMOrder*)order;
@end
