//
//  OrderTableViewCell.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "OrderTableViewCell.h"

@implementation OrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)fillWithOrder:(CCMOrder*)order{
    _statusLabel.text = [NSString stringWithFormat:@"Status : %@", order.status];
    _orderIdLabel.text = [NSString stringWithFormat:@"Commande N° : %ld", (long)order.orderID];
    _countPizzasLabel.text = [NSString stringWithFormat:@"1 pizzas"];
}
@end
