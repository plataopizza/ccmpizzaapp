//
//  PizzaTableViewCell.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMPizza.h"
typedef void(^DidPressOrderButton)();

@interface PizzaTableViewCell : UITableViewCell
@property (nonatomic, copy) DidPressOrderButton didPressOrderButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;
- (IBAction)orderAction:(id)sender;
- (void)fillWithPizza:(CCMPizza *)pizza timeOut:(BOOL)timeOut;
@end
