//
//  PizzaTableViewCell.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "PizzaTableViewCell.h"

@implementation PizzaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)orderAction:(id)sender {
    if (_didPressOrderButton) {
        _didPressOrderButton();
    }
}

- (void)fillWithPizza:(CCMPizza *)pizza timeOut:(BOOL)timeOut{
    _nameLabel.text = pizza.name;
    _priceLabel.text = [NSString stringWithFormat:@"%.02f euros", pizza.price];
    if (timeOut) {
        _orderButton.alpha = 0;
        _orderButton.enabled = NO;
    }else{
        _orderButton.alpha = 1;
        _orderButton.enabled = YES;
    }
}
@end
