//
//  UIWindow+PhoceisSDKiOS.h
//  Phoceis
//
//  Created by Phoceis on 29/02/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (PhoceisSDKiOS)

/**
 *  Returns the visible view controller of the window
 *
 *  @return The visible view controller
 */
- (id)getVisibleViewController;

/**
 *  Returns the visible view controller of the first window
 *
 *  @return The visible view controller
 */
+ (id)getApplicationVisibleViewController;

/**
 *  Returns the visible view controller from a given view controller
 *
 *  @param vc The given view controller
 *
 *  @return The visible view controller
 */
+ (id)getVisibleViewControllerFromViewController:(UIViewController *)vc;


@end
