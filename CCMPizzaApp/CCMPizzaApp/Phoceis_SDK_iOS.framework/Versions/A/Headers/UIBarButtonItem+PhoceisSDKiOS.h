//
//  UIBarButtonItem+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 02/05/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BarButtonActionBlock)();

@interface UIBarButtonItem (PhoceisSDKiOS)

/**
 *  A block that is run when the UIBarButtonItem is tapped.
 *
 *  @param actionBlock The action performed by tapping the bar button item
 */
- (void)setActionBlock:(BarButtonActionBlock)actionBlock;

@end
