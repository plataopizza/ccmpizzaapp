//
//  UIView+PhoceisSDKiOS.h
//
//  Created by Phoceis on 08/11/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    UIViewBordersStyle_RoundedBorders = 1,
    UIViewBordersStyle_RoundedBordersOnTop = 2,
    UIViewBordersStyle_RoundedBordersOnBottom = 3,
    UIViewBordersStyle_NoBorders = 0
} UIViewBordersStyle;

@interface UIView (PhoceisSDKiOS)

/**
 *  The border style of the view
 */
@property UIViewBordersStyle bordersStyle;

/**
 *  Round the view to form a circle
 */
- (void)doCircle;

/**
 *  Round the view to form a circle with customization elements
 *
 *  @param color       The border color of the view
 *  @param width       The width of the border
 *  @param withShadow  Add a shadow or not
 *  @param shadowColor The color of the shadow
 */
- (void)doCircleWithBorderColor:(UIColor *)color width:(CGFloat)width withShadow:(BOOL)withShadow shadowColor:(UIColor *)shadowColor;

/**
 *  Automatically round and shadow the view
 */
- (void)doRoundedShadowedView;

/**
 *  Round the corners of the view
 *
 *  @param color        The border color of the view
 *  @param width        The width of the border
 *  @param corderRadius The radius to round with
 */
- (void)doRoundedBordersWithBorderColor:(UIColor *)color width:(CGFloat)width cornerRadius:(CGFloat)corderRadius;

/**
 *  Rectangle the view and add colors to borders
 *
 *  @param color The color of the border
 *  @param width The width of the border
 */
- (void)doRectangleWithBorderColor:(UIColor *)color width:(CGFloat)width;

/**
 *  Add a soft shadow to the view
 *
 *  @param color        The color of the shadow
 *  @param shadowRadius The radius of the shadow
 */
- (void)doSoftShadowWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius;

/**
 *  Add a soft shadow to the view using CGPathWithColor
 *
 *  @param color        The color of the shadow
 *  @param shadowRadius The radius of the shadow
 */
- (void)doSoftShadowUsingCGPathWithColor:(UIColor *)color shadowRadius:(CGFloat)shadowRadius;

/**
 *  Add borders to the view
 *
 *  @param color       The color of the border
 *  @param borderWidth The width of the border
 *  @param onTop       Top border
 *  @param onBottom    Bottom border
 */
- (void)addBorderWithColor:(UIColor *)color borderWidth:(CGFloat)borderWidth onTop:(BOOL)onTop onBottom:(BOOL)onBottom;

/**
 *  Add borders to the view
 *
 *  @param color Color of the border
 *  @param frame Frame for the border
 */
- (void)addBorderWithColor:(UIColor *)color atFrame:(CGRect)frame;

/**
 *  Add borders from the border style property of the view
 */
- (void)setBordersFromBordersStyle;

/**
 *  End editing for all views and superviews
 *
 *  @param endEditing Force the endEditing of views
 */
- (void)recursiveEndEditing:(BOOL)endEditing;

/**
 *  Perform a shake animation on the view
 */
- (void)shake;

/**
 *  Take a snapshot of the view
 *
 *  @return A UIImage representation of the snapshot
 */
- (UIImage *)takeSnapshot;

/**
 *  Returns the parent view controller of the view
 *
 *  @return The parent view controller
 */
- (id)parentViewController;

/**
 *  Pushes back the view with an animation
 */
- (void)pushBack;

/**
 *  Pushes front the view with an animation
 */
- (void)pushFront;

/**
 *  Performs a wiggle animation on the view. Use stopWiggle to stop the animation
 */
- (void)startWiggle;

/**
 *  Stops the wiggle animation started with startWiggle
 */
- (void)stopWiggle;

- (void)doInnerShadow;
- (void)doInnerShadowWithRadius:(float)radius alpha:(float)alpha;
- (void)doInnerShadowWithRadius:(float)radius color:(UIColor *)color directions:(NSArray *)directions;

@end
