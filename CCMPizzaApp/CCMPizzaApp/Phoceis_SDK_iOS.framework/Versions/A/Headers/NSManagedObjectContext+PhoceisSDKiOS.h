//
//  NSManagedObjectContext+PhoceisSDKiOS.h
//
//  Created by Phoceis on 08/12/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (PhoceisSDKiOS)

/**
 *  Returns the first object found with a given predicate and entity name
 *
 *  @param predicate  The given predicate to use
 *  @param entityName The given entity name as string to use
 *
 *  @return The first object of the fetch query
 */
-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;

/**
 *  Returns the first object found with a given predicate and entity name and include or not its properties
 *
 *  @param predicate              The given predicate to use
 *  @param entityName             The given entity name as string to use
 *  @param includesPropertyValues Include or not the properties of the found object
 *
 *  @return The first object of the fetch query
 */
-(id)objectWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues;

/**
 *  Returns the first object found with a given predicate and entity description
 *
 *  @param predicate The given predicate to use
 *  @param entity    The entity to use (as NSEntityDescription)
 *
 *  @return The first object of the fetch query
 */
-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;

/**
 *  Returns the first object found with a given predicate and entity description and include or not its properties
 *
 *  @param predicate              The given predicate to use
 *  @param entity                 The entity to use (as NSEntityDescription)
 *  @param includesPropertyValues Include or not the properties of the found object
 *
 *  @return The first object of the fetch query
 */
-(id)objectWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues;

/**
 *  Returns an array of objects found with a given predicate and entity name
 *
 *  @param predicate  The given predicate to use
 *  @param entityName The given entity name as string to use
 *
 *  @return NSArray of the objects of the fetch query
 */
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;

/**
 *  Returns an array of objects found with a given predicate and entity name and include or not its properties
 *
 *  @param predicate              The given predicate to use
 *  @param entityName             The given entity name as string to use
 *  @param includesPropertyValues Include or not the properties of the found object
 *
 *  @return NSArray of the objects of the fetch query
 */
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName includesPropertyValues:(BOOL)includesPropertyValues;

/**
 *  Returns an array of objects found with a given predicate and entity description
 *
 *  @param predicate The given predicate to use
 *  @param entity    The entity to use (as NSEntityDescription)
 *
 *  @return NSArray of the objects of the fetch query
 */
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;

/**
 *  Returns an array of objects found with a given predicate and entity description and include or not its properties
 *
 *  @param predicate              The given predicate to use
 *  @param entity                 The entity to use (as NSEntityDescription)
 *  @param includesPropertyValues Include or not the properties of the found object
 *
 *  @return NSArray of the objects of the fetch query
 */
-(NSArray *)objectsWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity includesPropertyValues:(BOOL)includesPropertyValues;

/**
 *  Returns the number of objects found by a fetch query with a given predicate and entity name
 *
 *  @param predicate  The given predicate to use
 *  @param entityName The given entity name as string to use
 *
 *  @return Number of objects found
 */
-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntityName:(NSString *)entityName;

/**
 *  Returns the number of objects found by a fetch query with a given predicate and entity name
 *
 *  @param predicate The given predicate to use
 *  @param entity    The entity to use (as NSEntityDescription)
 *
 *  @return Number of objects found
 */
-(NSUInteger)countWithPredicate:(NSPredicate *)predicate forEntity:(NSEntityDescription *)entity;

@end
