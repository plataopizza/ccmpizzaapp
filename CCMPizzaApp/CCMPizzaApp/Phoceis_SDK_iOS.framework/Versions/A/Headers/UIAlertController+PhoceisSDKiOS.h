//
//  UIAlertController+PhoceisSDKiOS.h
//
//  Created by Phoceis on 04/03/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^OnAlertAction)(UIAlertController *alertController);

@interface UIAlertController (PhoceisSDKiOS)

/**
 *  Instantiates a simple AlertController with one button and customization elements
 *
 *  @param title         The title of the alert
 *  @param message       The message of the alert
 *  @param okButtonTitle The title of the only button
 *  @param onOKAction    The action of the only button
 *
 *  @return The AlertController instance customized
 */
+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction;

/**
 *  Instantiates a simple AlertController with two buttons and customization elements
 *
 *  @param title             The title of the alert
 *  @param message           The message of the alert
 *  @param okButtonTitle     The title of the Ok button
 *  @param cancelButtonTitle The title of the Cancel button
 *  @param onOKAction        The action of the Ok button
 *  @param onCancelAction    The action of the Cancel button
 *
 *  @return The AlertController instance customized
 */
+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction;

/**
 *  Instantiates and shows an AlertController with one button and customization elements
 *
 *  @param title         The title of the alert
 *  @param message       The message of the alert
 *  @param okButtonTitle The title of the only button
 *  @param onOKAction    The action of the only button
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction;

/**
 *  Instantes and shows an AlertController with one button and customization elements
 *
 *  @param title         The title of the alert
 *  @param message       The message of the alert
 *  @param okButtonTitle The title of the only button
 *  @param onOKAction    The action of the only button
 *  @param tintColor     The tint color of the alert (text color of button)
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle onOKAction:(OnAlertAction)onOKAction tintColor:(UIColor *)tintColor;

/**
 *  Instantiates and shows an AlertController with two buttons and customization elements
 *
 *  @param title             The title of the alert
 *  @param message           The message of the alert
 *  @param okButtonTitle     The title of the Ok button
 *  @param cancelButtonTitle The title of the Cancel button
 *  @param onOKAction        The action of the Ok button
 *  @param onCancelAction    The action of the Cancel button
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction;

/**
 *  Instantiates and shows an AlertController with two buttons and customization elements
 *
 *  @param title             The title of the alert
 *  @param message           The message of the alert
 *  @param okButtonTitle     The title of the Ok button
 *  @param cancelButtonTitle The title of the Cancel button
 *  @param onOKAction        The action of the Ok button
 *  @param onCancelAction    The action of the Cancel button
 *  @param tintColor         The tint color of the alert (text color of buttons)
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle onOKAction:(OnAlertAction)onOKAction onCancelAction:(OnAlertAction)onCancelAction tintColor:(UIColor *)tintColor;


@end
