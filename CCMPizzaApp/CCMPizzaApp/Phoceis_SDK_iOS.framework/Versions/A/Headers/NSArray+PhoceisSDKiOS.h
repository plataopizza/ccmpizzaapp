//
//  NSArray+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 02/05/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (PhoceisSDKiOS)
/**
 *  Iterate over each object of the array
 *
 *  @param block The action to be performed for each object
 */
- (void)each:(void (^)(id object))block;

/**
 *  Iterate over each object of the array and gives the current index
 *
 *  @param block The action to be performed for each object
 */
- (void)eachWithIndex:(void (^)(id object, NSUInteger index))block;
@end
