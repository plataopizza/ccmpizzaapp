//
//  MKMapView+PhoceisSDKiOS.h
//  Phoceis
//
//  Created by Phoceis on 03/03/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (PhoceisSDKiOS)

/**
 *  Reset the overlay and annotations of a MapView
 */
- (void)resetMapViewExceptUserLocation;

/**
 *  Center the view on the annotations of a MapView
 */
- (void)zoomToFitAnnotations;

@end
