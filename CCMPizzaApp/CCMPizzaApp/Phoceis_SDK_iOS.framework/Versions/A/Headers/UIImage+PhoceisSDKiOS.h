//
//  UIImage+PhoceisSDKiOS.h
//
//  Created by Phoceis on 01/12/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PhoceisSDKiOS)

/**
 *  Add a mask of a given color and shadow offset to the image
 *
 *  @param maskColor The color of the mask to use
 *  @param shadow    The shadow offset to use
 *
 *  @return An UIImage with a masked color
 */
- (UIImage *)imageMaskWithColor:(UIColor *)maskColor shadowOffset:(CGPoint)shadow;

/**
 *  Round the corners of the image for a given image view
 *
 *  @param imageView The image view for rounding the image
 *
 *  @return A rounded UIImage
 */
- (UIImage *)getRoundedImageOnReferenceView:(UIImageView*)imageView;

/**
 *  Fix the orientation of the image
 *
 *  @return An UIImage with correct orientation
 */
- (UIImage *)fixOrientation;

/**
 *  Adds gaussian blur to the image
 *
 *  @return An UIImage with gaussian blur
 */
- (UIImage *)imageWithGaussianBlur;

/**
 *  Fetch synchronously an image from an URL
 *
 *  @param urlString    The URL String to fetch the image from
 *  @param defaultImage The default image in case the fetch failed or the URL is not valid
 *
 *  @return The image fetched or the default image
 */
+ (UIImage *)imageWithURLString:(NSString *)urlString defaultImage:(UIImage *)defaultImage;

/**
 *  Fill an image with a given color from another image
 *
 *  @param source The image to fill
 *  @param color  The color used to fill the image
 *
 *  @return The source image filled with the given color
 */
+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color;

/**
 *  Change background color of an image to a given color instead of black
 *
 *  @param image     The image which the background is changed
 *  @param hexaColor The color to change black from
 *
 *  @return An UIImage with a different background color
 */
+ (UIImage *)changeBlackBackgroundImage:(UIImage*)image toHexaColor:(uint)hexaColor;

/**
 *  Crop an image to a given rect
 *
 *  @param image    The image to crop
 *  @param cropRect The rect to crop to
 *
 *  @return An UIImage cropped
 */
+ (UIImage *)croppedImage:(UIImage *)image cropRect:(CGRect)cropRect;

/**
 *  Resize an image to a given width and height
 *
 *  @param image  The image to resize
 *  @param width  The width to resize to
 *  @param height The height to resize to
 *
 *  @return An UIImage resized
 */
+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height;

/**
 *  Take a screenshot of a given view
 *
 *  @param aView The view to screenshot
 *
 *  @return An UIImage representation of the screenshot of the view
 */
+ (UIImage *)screenshotFromView:(UIView *)aView;

/**
 *  Change the color of a given image
 *
 *  @param image The image which the color is changed
 *  @param color The color to use
 *
 *  @return An UIImage of the given color from the given image source
 */
+ (UIImage *)image:(UIImage *)image withColor:(UIColor *)color;

/**
 *  Create a QRCode image from a given code with a given scale (1: blurry, use 2 or 3)
 *
 *  @param qrString The code of the QRCode
 *  @param scale    The scale of the rendered image
 *
 *  @return An UIImage representation of the QRCode
 */
+ (UIImage *)qrCodeForString:(NSString *)qrString withScale:(CGFloat)scale;

/**
 *  Create a non interpolated UIImage from a CIImage
 *
 *  @param image The given CIImage to use
 *  @param scale The scale to use
 *
 *  @return An UIImage representation of the given CIImage
 */
+ (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale;

/**
 *  Create a QRCode image from a given code
 *
 *  @param qrString The code of the QRCode
 *
 *  @return A CIImage representation of the QRCode
 */
+ (CIImage *)createQRForString:(NSString *)qrString;

/**
 *  Replace white color to transparance
 *
 *  @param image the image
 *
 *  @return the image
 */
+(UIImage *)changeWhiteColorTransparent: (UIImage *)image;


@end
