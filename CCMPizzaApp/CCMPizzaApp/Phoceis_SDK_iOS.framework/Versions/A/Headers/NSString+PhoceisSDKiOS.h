//
//  NSString+PhoceisSDKiOS.h
//
//  Created by Phoceis on 27/10/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    STTextDirectionNeutral = 0,
    STTextDirectionLeftToRight,
    STTextDirectionRightToLeft,
} STTextDirection;

typedef enum {
    IBANValidation_OK,					// IBAN is valid
    IBANValidation_UnknownCountry,		// IBAN is valid, but country is unknown. You may ignore this error.
    IBANValidation_Error,				// IBAN is invalid
} IBANValidationResult;

@interface NSString (PhoceisSDKiOS)

/**
 *  Returns the direction of a string
 *
 *  @return Direction of string of type STTextDirection
 */
- (STTextDirection)getBaseDirection;

/**
 *  Returns the formatted string as a legal URL type
 *
 *  @return The formatted string
 */
- (NSString *)getFormattedURL;

/**
 *  Returns whether the string is not nil and is not an empty String: @""
 *
 *  @return YES if the string is not empty and not nil, NO if the string is nil or equals to @""
 */
- (BOOL)isNotNilAndNotEmpty;

/**
 *  Returns whether the string is empty or not.
 *
 *  @return YES if empty, NO if not
 */
- (BOOL)isEmpty;

/**
 *  Returns whether the string can be used as a legal email adress
 *
 *  @return YES if the string is a valid email adress, NO if not
 */
- (BOOL)isValidEmail;

/**
 *  Returns wheter the string can be used as a legal IBAN
 *
 *  @return YES if the string is a valid IBAN, NO if not
 */
- (BOOL)isValidIBAN;

/**
 *  Returns the Rect of a string restricted to a given width for a given font
 *
 *  @param font  The given font to use
 *  @param width The given restricted width to use
 *
 *  @return The CGRect of the string
 */
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;

/**
 *  Returns the Rect of a string restricted to a given height for a given font
 *
 *  @param font   The given font to use
 *  @param height The given restricted height to use
 *
 *  @return The CGRect of the string
 */
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;

/**
 *  Returns the Rect of a string restricted to a given width and height for a given font
 *
 *  @param font   The given font to use
 *  @param width  The given restricted width to use
 *  @param height The given restricted height to use
 *
 *  @return The CGRect of the string
 */
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width constrainedToHeight:(CGFloat)height;

/**
 *   Returns the Rect of a string restricted to a given size for a given font
 *
 *  @param font            The given font to use
 *  @param constrainedSize The given restricted size to use
 *
 *  @return The CGRect of the string
 */
- (CGRect)getBoundingRectWithFont:(UIFont *)font constrainedSize:(CGSize)constrainedSize;

/**
 *  Check wheter the string is a string representation of a number
 *
 *  @return YES if the string is a number, NO if not
 */
- (BOOL)isNumber;

/**
 *  Returns the number of lines a string would take in a given restricted size
 *
 *  @param font            The given font to use
 *  @param constrainedSize The given restricted size to use
 *
 *  @return The number of lines
 */
- (NSInteger)numberOfLinesWithFont:(UIFont *)font constrainedSize:(CGSize)constrainedSize;

/**
 *  Trim a string from white spaces and new lines characters
 *
 *  @param string The string to trim
 *
 *  @return The trimmed string
 */
+ (NSString *)trimString:(NSString *)string;

/**
 *  Crypt string with SHA256...
 *
 *  @return The SHA256 string...
 */
- (NSString *)sha256;

@end
