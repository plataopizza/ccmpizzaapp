//
//  PhoceisKeyboardAvoidingTableView.h
//  Phoceis SDK iOS
//

#import <UIKit/UIKit.h>
#import "UIScrollView+PhoceisKeyboardAvoidingAdditions.h"

@interface PhoceisKeyboardAvoidingTableView : UITableView<UITextFieldDelegate, UITextViewDelegate>

- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
