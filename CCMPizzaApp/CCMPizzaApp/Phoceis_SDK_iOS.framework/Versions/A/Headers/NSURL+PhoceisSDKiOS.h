//
//  NSURL+PhoceisSDKiOS.h
//
//  Created by Phoceis on 29/02/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (PhoceisSDKiOS)

/**
 *  Returns the value of a given parameter of the url
 *
 *  @param parameter The parameter whose value will be returned
 *
 *  @return The value of the given parameter as a String
 */
- (NSString *)getValueForParameter:(NSString *)parameter;

/**
 *  Returns a dictionary of all parameters of the url
 *
 *  @return A Dictionary of all the parameters
 */
- (NSDictionary *)getParametersDictionary;

@end
