//
//  UIStoryboard+PhoceisSDKiOS.h
//
//  Created by Phoceis on 02/02/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (PhoceisSDKiOS)

/**
 *  Instantiate the initial view controller of a given storyboard
 *
 *  @param name The name of the storyboard
 *
 *  @return An instance of the initial view controller of the given storyboard, nil if none found
 */
+ (UIViewController *)instantiateInitialControllerWithStoryboardName:(NSString *)name;

/**
 *  Instantiate a view controller with a given name of a given storyboard
 *
 *  @param viewControllerName The view controller name as string
 *  @param storyBoardName     The storyboard name as string
 *
 *  @return An instance of the desired view controller in the desired storyboard, nil if none found
 */
+ (UIViewController*)instantiateViewControllerWithName:(NSString*)viewControllerName fromStoryBoardName:(NSString*)storyBoardName;

/**
 *  Instantiate a view controller with a given identifier of a given storyboard
 *
 *  @param viewControllerIdentifier The view controller identifier as string
 *  @param storyboardName           The storyboard name as string
 *
 *  @return An instance of the desired view controller in the desired storyboard, nil if none found
 */
+ (id)getViewControllerWithIdentifier:(NSString *)viewControllerIdentifier inStoryboardWithName:(NSString *)storyboardName;

@end
