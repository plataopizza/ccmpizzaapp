//
//  NSMutableURLRequest+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 24/06/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (PhoceisSDKiOS)

/**
 *  Ajoute un dictionnaire Json à la requête avec une HTTP methode donnée
 *
 *  @param jsonDictionary Le dictionnaire Json à ajouter
 *  @param method         La méthode HTTP donnée
 */
- (void)addJsonDictionary:(NSDictionary *)jsonDictionary withHTTPMethod:(NSString *)method;
@end
