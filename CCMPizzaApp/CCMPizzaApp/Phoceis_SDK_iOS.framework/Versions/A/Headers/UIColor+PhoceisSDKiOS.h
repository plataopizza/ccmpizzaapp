//
//  UIColor+PhoceisSDKiOS.h
//  Phoceis
//
//  Created by Phoceis on 07/03/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PhoceisSDKiOS)

/**
 *  Returns whether the color is the same as another color with a given tolerence
 *
 *  @param color     The other color to compare with
 *  @param tolerance The tolerence (acceptance) for which the color is considered the same
 *
 *  @return YES, the color is the same, NO, the color is different
 */
- (BOOL)isEqualToColor:(UIColor *)color withTolerance:(CGFloat)tolerance;

/**
 *  Returns an UIColor for a given Hex string, with or without alpha component
 *
 *  @param hexString   The given hex String to use
 *  @param ignoreAlpha Ignore or not the alpha component
 *
 *  @return The UIColor corresponding to an Hex string
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString ignoreAlpha:(BOOL)ignoreAlpha;

@end
