//
//  NSBundle+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 24/06/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (PhoceisSDKiOS)


/**
 *  Returns the version number of the application
 *
 *  @return Short Version Number
 */
-(NSString *)versionNumber;

/**
 *  Returns the build number of the application
 *
 *  @return Build Number
 */
-(NSInteger)buildNumber;

/**
 *  Returns the bundle identifier of the application
 *
 *  @return Bundle Identifier
 */
-(NSString *)bundleIdentifier;

/**
 *  Returns the current language
 *
 *  @return Current Language
 */
-(NSString *)currentLanguage;

/**
 *  Returns the device model
 *
 *  @return Device Model
 */
-(NSString *)deviceModel;

@end
