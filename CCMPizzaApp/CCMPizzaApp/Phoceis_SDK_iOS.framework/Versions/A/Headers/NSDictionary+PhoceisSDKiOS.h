//
//  NSDictionary+PhoceisSDKiOS.h
//
//  Created by Phoceis on 27/10/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PhoceisSDKiOS)

/**
 *  Returns whether or not the dictionnary contains a given key
 *
 *  @param key The key to look for
 *
 *  @return YES if the dictionary contains the given key, NO if not
 */
- (BOOL)containsKey:(NSString *)key;

/**
 *  Returns a string of a json representation of the dictionary
 *
 *  @param prettyPrint Use PrettyPrint to make the json more readable (adds spaces)
 *
 *  @return The string of a json representation of the dictionary
 */
- (NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint;

/**
 *  Iterate over each key/value entry of the dictionary
 *
 *  @param block The action to perform for each key/value entry
 */
- (void)each:(void (^)(id k, id v))block;

/**
 *  Iterate over each key of the dictionary
 *
 *  @param block The action to perform for each key
 */
- (void)eachKey:(void (^)(id k))block;

/**
 *  Iterate over each value of the dictionary
 *
 *  @param block The action perform for each value
 */
- (void)eachValue:(void (^)(id v))block;
@end
