//
//  UIScrollView+PhoceisKeyboardAvoidingAdditions.h
//  Phoceis SDK iOS
//

#import <UIKit/UIKit.h>

@interface UIScrollView (PhoceisKeyboardAvoidingAdditions)

- (BOOL)keyboardAvoiding_focusNextTextField;
- (void)keyboardAvoiding_scrollToActiveTextField;
- (void)keyboardAvoiding_keyboardWillShow:(NSNotification *)notification;
- (void)keyboardAvoiding_keyboardWillHide:(NSNotification *)notification;
- (void)keyboardAvoiding_updateContentInset;
- (void)keyboardAvoiding_updateFromContentSizeChange;
- (void)keyboardAvoiding_assignTextDelegateForViewsBeneathView:(UIView *)view;
- (UIView *)keyboardAvoiding_findFirstResponderBeneathView:(UIView *)view;
- (CGSize)keyboardAvoiding_calculatedContentSizeFromSubviewFrames;

@end
