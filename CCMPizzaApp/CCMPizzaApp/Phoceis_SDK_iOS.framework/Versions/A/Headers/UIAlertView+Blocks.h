//
//  UIAlertView+Blocks.h
//
//  Created by Phoceis on 27/10/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Blocks) <UIAlertViewDelegate>

/**
 *  Instantiates and shows an UIAlertView with customization (deprecated, use UIAlertController)
 *
 *  @param title             The title of the alert
 *  @param message           The message of the alert
 *  @param clickHandler      The action of a button
 *  @param cancelButtonTitle The title of the cancel button
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle __attribute__((deprecated));

/**
 *  Instantiates and shows an UIAlertView with customization (deprecated, use UIAlertController)
 *
 *  @param title             The title of the alert
 *  @param message           The message of the alert
 *  @param clickHandler      The action of a button
 *  @param cancelButtonTitle The title of the cancel button
 *  @param otherButtonTitles The titles of the other buttons
 */
+ (void)showWithTitle:(NSString *)title message:(NSString *)message clickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION __attribute__((deprecated));

/**
 *  Instantiates and shows an UIAlertView (deprecated, use UIAlertController)
 *
 *  @param clickHandler The action of a button
 */
- (void)showWithClickHandler:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))clickHandler __attribute__((deprecated));

@end
