//
//  UIViewController+PhoceisSDKiOS.h
//
//  Created by Phoceis on 17/06/2015.
//  Copyright (c) 2015 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PhoceisSDKiOS)

/**
 *  Returns whether or not the view controller is presented modally
 *
 *  @return YES if the view controller is modal, NO if not
 */
- (BOOL)isModal;

/**
 *  Perform a pushBack animation on the main view when presenting a view controller
 */
- (void)pushBackAnimation;

/**
 *  Perform a pushFront animation on the main view when popping a view controller
 */
- (void)pushFrontAnimation;

@end
