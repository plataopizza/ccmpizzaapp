//
//  NSIndexPath+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 02/05/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSIndexPath (PhoceisSDKiOS)
/**
 *  Returns the preview row of the index path
 *
 *  @return The preview row of the index path
 */
- (NSIndexPath *)previousRow;

/**
 *  Returns the next row of the index path
 *
 *  @return The next row of the index path
 */
- (NSIndexPath *)nextRow;

/**
 *  Returns the previous item of the index path
 *
 *  @return The previous item of the index path
 */
- (NSIndexPath *)previousItem;

/**
 *  Returns the next item of the index path
 *
 *  @return The next item of the index path
 */
- (NSIndexPath *)nextItem;

/**
 *  Returns the next section of the index path
 *
 *  @return The next section of the index path
 */
- (NSIndexPath *)nextSection;

/**
 *  Returns the previous section of the index path
 *
 *  @return The previous section of the index path
 */
- (NSIndexPath *)previousSection;
@end
