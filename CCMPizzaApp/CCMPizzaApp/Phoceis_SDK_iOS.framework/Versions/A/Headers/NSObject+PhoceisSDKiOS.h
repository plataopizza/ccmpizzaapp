//
//  NSObject+PhoceisSDKiOS.h
//
//  Created by Phoceis on 27/10/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface NSObject (PhoceisSDKiOS)

/**
 *  Returns the class name of the object
 *
 *  @return The class name as a string
 */
- (NSString *)className;

/**
 *  Returns the type of a given property
 *
 *  @param property The given property
 *
 *  @return The type of the property as a string
 */
- (NSString *)typeFromProperty:(objc_property_t)property;

/**
 *  Populate the properties of an object with its corresponding entries in a given json
 *
 *  @param jsonDictionary The given json as a Dictionary
 */
- (void)populateWithJsonDictionary:(NSDictionary *)jsonDictionary;

/**
 *  Replace NSNull by nil
 *
 *  @return The instance of the object, or nil
 */
- (id)replaceNSNullByNil;

/**
 *  Returns the properties of a the object
 *
 *  @return The properties as an NSDictionary
 */
- (NSDictionary *)properties;

/**
 *  Auto-Encode the properties of an object for each type with given coder
 *
 *  @param aCoder The given coder to use
 */
- (void)encodeAutoWithCoder:(NSCoder *)aCoder;

/**
 *  Auto-Decode the properties of an object for each type with given coder
 *
 *  @param aDecoder The given coder to use
 */
- (void)decodeAutoWithCoder:(NSCoder *)aDecoder;

/**
 *  Auto-Encode the properties of an object of given class for each type with given coder
 *
 *  @param aCoder      The given coder to use
 *  @param classObject The class of the object
 */
- (void)encodeAutoWithCoder:(NSCoder *)aCoder class:(Class)classObject;

/**
 *  Auto-Decode the properties of an object of given class for each type with given coder
 *
 *  @param aDecoder    The given coder to use
 *  @param classObject The class of the object
 */
- (void)decodeAutoWithCoder:(NSCoder *)aDecoder class:(Class)classObject;

@end
