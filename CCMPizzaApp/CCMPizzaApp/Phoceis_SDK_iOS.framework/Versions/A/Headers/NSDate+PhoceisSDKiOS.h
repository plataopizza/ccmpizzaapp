//
//  NSDate+PhoceisSDKiOS.h
//
//  Created by Phoceis on 27/10/2015.
//  Copyright © 2015 Phoceis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PhoceisSDKiOS)

/**
 *  Gives the date without the hours component
 *
 *  @return date without hour component
 */
- (NSDate *)dateWithoutTime;

/**
 *  Gives the date without the year and hours components
 *
 *  @return date without years and hours components
 */
- (NSDate *)dateWithoutYear;

/**
 *  Add a given number of days to a date
 *
 *  @param days Number of days to add
 *
 *  @return The modified date
 */
- (NSDate *)dateByAddingDays:(NSInteger) days;

/**
 *  Add a given number of months to a date
 *
 *  @param months Number of months to add
 *
 *  @return the modified date
 */
- (NSDate *)dateByAddingMonths:(NSInteger) months;

/**
 *  Add a given number of years to a date
 *
 *  @param years Number of years to add
 *
 *  @return the modified date
 */
- (NSDate *)dateByAddingYears:(NSInteger) years;

/**
 *  Add a given number of days, months and years
 *
 *  @param days   Number of days to add
 *  @param months Number of months to add
 *  @param years  Number of years to add
 *
 *  @return the modified date
 */
- (NSDate *)dateByAddingDays:(NSInteger) days months:(NSInteger) months years:(NSInteger) years;

/**
 *  Returns the date of the first day of the month
 *
 *  @return date of the first day of the month
 */
- (NSDate *)monthStartDate;

/**
 *  Returns the number of days for this month
 *
 *  @return number of days
 */
- (NSUInteger)numberOfDaysInMonth;

/**
 *  Returns the number of the day in the week (Sunday = 1) for gregorian calendar
 *
 *  @return weakday in gregorian calendar
 */
- (NSUInteger)weekday;

/**
 *  Returns the number of days since a given date
 *
 *  @param date The date to be compared to
 *
 *  @return number of days
 */
- (NSInteger)daysSinceDate:(NSDate *) date;

/**
 *  Returns the date as an NSString
 *
 *  @param format The format to be used to generate the string (ex: "yyyy-MM-dd HH:mm:ss zzz")
 *
 *  @return The date as a string
 */
- (NSString *)dateStringWithFormat:(NSString *)format;

/**
 *  Returns the date with added hours, minutes and seconds
 *
 *  @param time Time to add with format: "HH:mm:ss"
 *
 *  @return The date with time component
 */
- (NSDate *)dateBySettingTime:(NSString *)time;

/**
 *  Returns the current date as a string with format: "yyyy-MM-dd HH:mm:ss"
 *
 *  @return Current date as string
 */
+ (NSString *)getNowDateString;

/**
 *  Returns string of a given date with format: "yyyy-MM-dd HH:mm:ss"
 *
 *  @param date The given date
 *
 *  @return The given date as a string
 */
+ (NSString *)getDateStringFromDate:(NSDate *)date;

/**
 *  Returns string of a given date with a given format
 *
 *  @param date   The given date
 *  @param format The given format
 *
 *  @return The given date with given format as a string
 */
+ (NSString *)getDateStringFromDate:(NSDate *)date withFormat:(NSString *)format;

/**
 *  Returns string of a given date with a given format and a given locale
 *
 *  @param date   The given date
 *  @param format The given format
 *  @param locale The given locale (ex: en_US)
 *
 *  @return The given date with given format and locale as a string
 */
+ (NSString *)getDateStringFromDate:(NSDate *)date withFormat:(NSString *)format withLocale:(NSLocale *)locale;

/**
 *  Returns string of a given a date with format : "yyyy-MM-dd"
 *
 *  @param date The given date
 *
 *  @return The given date as a short string
 */
+ (NSString *)getShortDateStringFromDate:(NSDate *)date;

/**
 *  Returns string of a given date in string format
 *
 *  @param date           The given date to format
 *  @param fromFormatDate The format to use
 *  @param longStyle      Include the time or not
 *  @param withYear       Include the years or not
 *
 *  @return The given date formatted
 */
+ (NSString *)getFormattedDateStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate longStyle:(BOOL)longStyle withYear:(BOOL)withYear;

/**
 *  Returns the time of a given date with format: "HH:mm"
 *
 *  @param date           The given date from which the time is extracted
 *  @param fromFormatDate The format of the given date
 *  @param separator      The separator to use between hours and minutes
 *
 *  @return The time as a string
 */
+ (NSString *)getFormattedTimeStringFromDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate separator:(NSString *)separator;

/**
 *  Change the format of a date and returns the date as a string
 *
 *  @param date           The given date to change
 *  @param fromFormatDate The current format of the given date
 *  @param toFormatDate   The given format to change the date with
 *
 *  @return The changed date as a string
 */
+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate;

/**
 *  Change the format of a date and returns the date as a string, with "Tomorrow" , "Today", and "Yesterday" if the date corresponds to these dates
 *
 *  @param date                     The given date to change
 *  @param fromFormatDate           The format of the given date
 *  @param toFormatDate             The format used to change the date
 *  @param localizedYesterdayString Localization of "Yesterday"
 *  @param localizedTodayString     Localization of "Today"
 *  @param localizedTomorrowString  Localization of "Tomorrow"
 *
 *  @return The changed date as a string
 */
+ (NSString *)getFormattedDateStringFromFormatedDateString:(NSString *)date fromFormatDate:(NSString *)fromFormatDate toFormatDate:(NSString *)toFormatDate localizedYesterdayString:(NSString *)localizedYesterdayString localizedTodayString:(NSString *)localizedTodayString localizedTomorrowString:(NSString *)localizedTomorrowString;

/**
 *  Clamp a date to given components
 *
 *  @param date      The date to clamp
 *  @param unitFlags UnitFlags for components to keep/use
 *
 *  @return The clamped date
 */
+ (NSDate *)clampDate:(NSDate *)date toComponents:(NSUInteger)unitFlags;

/**
 *  Clamp a date and compare it with another one
 *
 *  @param date          The date to clamp
 *  @param referenceDate The date to compare the first one with
 *
 *  @return YES if the dates are the same, NO if the dates are different
 */
+ (BOOL)clampAndCompareDate:(NSDate *)date withReferenceDate:(NSDate *)referenceDate;

/**
 *  Returns a date from a given string with a given format
 *
 *  @param string The given date as string
 *  @param format The format to use
 *
 *  @return The date as NSDate from the given string
 */
+ (NSDate *)getDateFromString:(NSString *)string withFormat:(NSString *)format;

@end
