//
//  PhoceisKeyboardAvoidingCollectionView.h
//  Phoceis SDK iOS
//

#import <UIKit/UIKit.h>
#import "UIScrollView+PhoceisKeyboardAvoidingAdditions.h"

@interface PhoceisKeyboardAvoidingCollectionView : UICollectionView <UITextFieldDelegate, UITextViewDelegate>

- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
