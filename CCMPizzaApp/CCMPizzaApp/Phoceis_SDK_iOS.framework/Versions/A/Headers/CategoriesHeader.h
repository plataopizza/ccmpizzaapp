//
//  CategoriesHeader.h
//  PhoceisVisionDemo
//
//  Created by Phoceis on 27/01/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#ifndef CategoriesHeader_h
#define CategoriesHeader_h

#import <UIKit/UIKit.h>
#import "NSString+PhoceisSDKiOS.h"
#import "MKMapView+PhoceisSDKiOS.h"
#import "NSDate+PhoceisSDKiOS.h"
#import "NSDictionary+PhoceisSDKiOS.h"
#import "NSManagedObjectContext+PhoceisSDKiOS.h"
#import "NSObject+PhoceisSDKiOS.h"
#import "NSURL+PhoceisSDKiOS.h"
#import "UIColor+PhoceisSDKiOS.h"
#import "UIAlertView+Blocks.h"
#import "UIButton+PhoceisSDKiOS.h"
#import "UIImage+PhoceisSDKiOS.h"
#import "UIStoryboard+PhoceisSDKiOS.h"
#import "UIView+AutoLayout.h"
#import "UIView+PhoceisSDKiOS.h"
#import "UIViewController+PhoceisSDKiOS.h"
#import "UIWindow+PhoceisSDKiOS.h"
#import "UIAlertController+PhoceisSDKiOS.h"
#import "NSArray+PhoceisSDKiOS.h"
#import "NSFileManager+PhoceisSDKiOS.h"
#import "NSIndexPath+PhoceisSDKiOS.h"
#import "UIBarButtonItem+PhoceisSDKiOS.h"

#endif /* CategoriesHeader_h */
