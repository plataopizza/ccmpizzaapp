//
//  UIButton+PhoceisSDKiOS.h
//
//  Created by Phoceis on 26/02/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (PhoceisSDKiOS)

/**
 *  Center the image and the title of the button
 */
- (void)centerImageAndText;

/**
 *  Center the image and the title of the button with a given padding
 *
 *  @param padding The given padding to use
 */
- (void)centerImageAndTextWithPadding:(float)padding;

@end
