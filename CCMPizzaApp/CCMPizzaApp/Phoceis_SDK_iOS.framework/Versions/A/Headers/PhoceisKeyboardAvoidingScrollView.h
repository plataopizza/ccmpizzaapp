//
//  PhoceisKeyboardAvoidingScrollView.h
//  Phoceis SDK iOS
//

#import <UIKit/UIKit.h>
#import "UIScrollView+PhoceisKeyboardAvoidingAdditions.h"

@interface PhoceisKeyboardAvoidingScrollView : UIScrollView <UITextFieldDelegate, UITextViewDelegate>

- (void)contentSizeToFit;
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
