//
//  UIDevice+PhoceisSDKiOS.h
//  Phoceis_SDK_iOS
//
//  Created by Valentin Denis on 24/06/2016.
//  Copyright © 2016 Phoceis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (PhoceisSDKiOS)
/**
 *  Returns the iOS version of the device
 *
 *  @return iOS Version
 */
+ (NSString *)systemVersion;

/**
 *  Returns whether or not the device has a camera
 *
 *  @return YES : has a camera, NO: doesn't
 */

+ (BOOL)hasCamera;
@end
