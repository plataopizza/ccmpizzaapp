//
//  CCMObject.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMObject.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>

@implementation CCMObject
//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions publiques...
//--------------------------------------------------------------------------------------------------
//Initialisation à partir d'un dictionnaire...
- (id)initWithJsonObject:(id)jsonObject{
    
    if (!jsonObject || [jsonObject isKindOfClass:[NSNull class]]){
        return nil;
    }
    
    if (self == [super init]){
        //if (DEBUG){
        if (jsonObject && ![jsonObject isKindOfClass:[NSNull class]]){
            NSLog(@"%@", jsonObject);
        }
        //}
    }
    return self;
    
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Encode/Decode...
//--------------------------------------------------------------------------------------------------
//Encodage, décodage...
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if(self) {
        [self decodeAutoWithCoder:decoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [self encodeAutoWithCoder:encoder];
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Fonctions partagées...
//--------------------------------------------------------------------------------------------------
//Récupère un array d'objets TAOObject (ou objets héritants) à partir d'un array json...
+ (NSMutableArray *)getArrayFromJsonObject:(id)jsonObject{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
        //L'objet json est bien un array...
        for (id item in jsonObject){
            id object  = [[NSClassFromString(self.class.className) alloc] initWithJsonObject:item];
            if (object) [array addObject:object];
        }
    }
    else if (jsonObject){
        //L'objet json est un objet unique, on renvoit tout de même un array, mais avec un seul objet...
        //Ne devrait pas arriver, sauf si le WS décide de ne plus envoyer d'array pour un objet unique...
        id object = [[NSClassFromString(self.class.className) alloc] initWithJsonObject:jsonObject];
        if (object) [array addObject:object];
    }
    return array;
}
@end
