//
//  CCMObject.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface CCMObject : RLMObject
- (id)initWithJsonObject:(id)jsonObject;
+ (NSMutableArray *)getArrayFromJsonObject:(id)jsonObject;
@end
