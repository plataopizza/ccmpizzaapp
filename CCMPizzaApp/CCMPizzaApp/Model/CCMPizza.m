//
//  CCMPizza.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMPizza.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>

@implementation CCMPizza
- (id)initWithJsonObject:(id)jsonObject{
    
    if (self == [super initWithJsonObject:jsonObject]){
        _pizzaID = [[jsonObject[@"id"] replaceNSNullByNil] integerValue];
        _price = [[jsonObject[@"price"] replaceNSNullByNil] floatValue]/100.0f;
        _name = [jsonObject[@"name"] replaceNSNullByNil];
        
    }
    return self;
    
}
@end
