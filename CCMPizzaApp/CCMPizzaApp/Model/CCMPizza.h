//
//  CCMPizza.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCMObject.h"

@interface CCMPizza : CCMObject
@property NSInteger pizzaID;
@property float price;
@property (nonatomic, retain) NSString *name;
@end

RLM_ARRAY_TYPE(CCMPizza)
