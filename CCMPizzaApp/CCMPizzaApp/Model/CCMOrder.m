//
//  CCMOrder.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMOrder.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>

@implementation CCMOrder
- (id)initWithJsonObject:(id)jsonObject{
    
    if (self == [super initWithJsonObject:jsonObject]){
        _orderID = [[jsonObject[@"id"] replaceNSNullByNil] integerValue];
        _status = [jsonObject[@"status"] replaceNSNullByNil];
        if (jsonObject[@"pizza"]) {
            if (jsonObject[@"pizza"][@"id"]) {
                RLMResults *results = [CCMPizza objectsWhere:@"pizzaID == %@", jsonObject[@"pizza"][@"id"]];
                if (results.count >=1) {
                    _item = [results objectAtIndex:0];
                }
            }
        }
    }
    return self;
    
}
@end
