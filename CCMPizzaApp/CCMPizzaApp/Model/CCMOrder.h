//
//  CCMOrder.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCMPizza.h"
#import "CCMObject.h"

@interface CCMOrder : CCMObject
@property NSInteger orderID;
@property CCMPizza *item;
@property (nonatomic, retain) NSString *status;
@end
