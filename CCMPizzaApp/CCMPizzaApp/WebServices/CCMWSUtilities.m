//
//  CCMWSUtilities.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMWSUtilities.h"
#import <CommonCrypto/CommonDigest.h>
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>
#import "CurrentContext.h"

static NSString *kLSWSUtilities_ProgressBlock_Key = @"LSWSUtilitiesProgressBlock";
static NSInteger timeOutTime = 3;

@interface CCMWSUtilities (Private)<NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate>

+ (NSURLSessionDataTask *)getFormatedDataTaskWithRequest:(NSMutableURLRequest * __strong *)request session:(NSURLSession *)session completion:(void (^)(NSInteger statusCode, id returnedJsonObject, NSError *error))completion;
- (void)secureReleaseProgressBlockIfExist;

@end

@interface CCMWSUtilities()
@property float expectedBytes;
@property (nonatomic, strong) NSMutableData *receivedData;
@end
@implementation CCMWSUtilities
#pragma mark - Singleton...
//Singleton...
+ (CCMWSUtilities *)sharedInstance{
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

+ (void)addHeaderForRequest:(NSMutableURLRequest * __strong *)request{
    if (![(*request) isKindOfClass:[NSMutableURLRequest class]]) return;

    [*request setValue:@"plataopizza" forHTTPHeaderField:@"Authorization"];
}

//Créée une data task utilisée pour les requêtes asynchrones...
+ (NSURLSessionDataTask *)getFormatedDataTaskWithRequest:(NSMutableURLRequest * __strong *)request session:(NSURLSession *)session completion:(void (^)(NSInteger, id, NSError *))completion{
    
    // EN RE7 DEBUG HEADER
    [CCMWSUtilities addHeaderForRequest:request];
    
    //Création de la data task...
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:*request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //Ok...
        NSInteger statusCode = 404;
        
        //On récupère le statusCode...
        if ([response isKindOfClass:[NSHTTPURLResponse class]]){
            statusCode = ((NSHTTPURLResponse *)response).statusCode;
        }else{
            statusCode = error.code;
        }
        
        if (error.code == -1001) {
            [CurrentContext sharedInstance].countOfFailedCalls = [CurrentContext sharedInstance].countOfFailedCalls + 1;
        }
        //Récupértion de la réponse...
        id returnedJsonObject = data?[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]:nil;
        if (statusCode >= 200 && statusCode <= 299 && data){
            //La requête a été exécutée avec succès...
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion){
                    completion(kLS_Succeed_Status_Code, returnedJsonObject, nil);
                }
            });
        }
        else {
            //La requête a échoué...
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion){
                    completion(statusCode, nil, error);
                }
            });
        }
        
    }];
    
    return dataTask;
    
}

//Secure release du block de progression s'il existe...
- (void)secureReleaseProgressBlockIfExist{
    void(^progressBlock)(CGFloat) = objc_getAssociatedObject([CCMWSUtilities sharedInstance], (__bridge const void *)(kLSWSUtilities_ProgressBlock_Key));
    if (progressBlock){
        progressBlock = nil;
        objc_setAssociatedObject([CCMWSUtilities sharedInstance], (__bridge const void *)(kLSWSUtilities_ProgressBlock_Key), nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

//Envoie de manière asynchrone une requête...
+ (void)sendAsynchronousRequest:(NSMutableURLRequest *__strong *)request completion:(void (^)(NSInteger statusCode, id returnedJsonObject, NSError *error))completion{
    
    //On lance de manière asynchrone la requête...
    NSLog(@"Appel WS : %@", (*request).URL.debugDescription);
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = timeOutTime;
    config.timeoutIntervalForResource = timeOutTime;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];

    [[CCMWSUtilities getFormatedDataTaskWithRequest:request session:session completion:completion] resume];
    
}

//Envoie une requête asynchrone et indique la progression...
//TODO le progress block ne fonctionne pas en get (voir delegates methods)
+ (void)sendAsynchronousRequest:(NSMutableURLRequest * __strong *)request progressBlock:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSInteger statusCode, id returnedJsonObject, NSError *error))completion{
        
    //On stocke le block de progress...
    objc_setAssociatedObject([CCMWSUtilities sharedInstance], (__bridge const void *)(kLSWSUtilities_ProgressBlock_Key), progressBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    //On lance la requête...
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSession sharedSession].configuration delegate:[CCMWSUtilities sharedInstance] delegateQueue:nil];
    
    void(^completionBlock)() = ^(NSInteger statusCode, id returnedJsonObject, NSError *error){
        [[CCMWSUtilities sharedInstance] secureReleaseProgressBlockIfExist];
        if (completion) completion(statusCode, returnedJsonObject, error);
    };
    
    NSURLSessionDataTask *task = [CCMWSUtilities getFormatedDataTaskWithRequest:request session:session completion:completionBlock];
    
    [task resume];
    
}



//--------------------------------------------------------------------------------------------------
#pragma mark - NSURLSessionDataDelegate methods...
//--------------------------------------------------------------------------------------------------

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    //On indique au block de progress la progression actuelle...
    void(^progressBlock)(CGFloat) = objc_getAssociatedObject([CCMWSUtilities sharedInstance], (__bridge const void *)(kLSWSUtilities_ProgressBlock_Key));
    if (progressBlock){
        NSNumber *tbs = [NSNumber numberWithInteger:(NSInteger)totalBytesSent];
        NSNumber *tbets = [NSNumber numberWithInteger:(NSInteger)totalBytesExpectedToSend];
        CGFloat progress = [tbs floatValue] / [tbets floatValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            progressBlock(progress);
        });
    }
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
    NSHTTPURLResponse *responseHTTP = (NSHTTPURLResponse *)response;
    _expectedBytes = 0;
    _receivedData = [_receivedData initWithLength:0];
    _expectedBytes = [responseHTTP expectedContentLength];
    if (completionHandler) {
        completionHandler(NSURLSessionResponseAllow);
    }
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
    [_receivedData appendData:data];
    CGFloat progressive = (float)[_receivedData length] / (float)_expectedBytes;
    //On indique au block de progress la progression actuelle...
    void(^progressBlock)(CGFloat) = objc_getAssociatedObject([CCMWSUtilities sharedInstance], (__bridge const void *)(kLSWSUtilities_ProgressBlock_Key));
    if (progressBlock){
        dispatch_async(dispatch_get_main_queue(), ^{
            progressBlock(progressive);
        });
    }
}

@end
