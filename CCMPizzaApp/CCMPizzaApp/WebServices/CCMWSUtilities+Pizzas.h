//
//  CCMWSUtilities+Pizzas.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMWSUtilities.h"
#import "CCMPizza.h"

@interface CCMWSUtilities (Pizzas)
+ (void)getAllPizzas:(void(^)(BOOL succeed, NSMutableArray<CCMPizza*>* pizzas, BOOL timedOut))completion;
@end
