//
//  CCMWSUtilities+Orders.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMWSUtilities.h"
#import "CCMOrder.h"

@interface CCMWSUtilities (Orders)
+ (void)getAllOrders:(void(^)(BOOL succeed, NSMutableArray<CCMOrder*>*orders, BOOL timedOut))completion;
+ (void)getOrderWithID:(NSString *)identifier completion:(void(^)(BOOL succeed, CCMOrder *order, BOOL timedOut))completion;
+ (void)postOrder:(CCMPizza *)pizza completion:(void(^)(BOOL succeed, CCMOrder *order, BOOL timedOut))completion;
@end
