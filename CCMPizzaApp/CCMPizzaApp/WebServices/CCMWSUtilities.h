//
//  CCMWSUtilities.h
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

static long const kLS_Succeed_Status_Code = 200;
static long const kLS_Succeed_NoData_Code = 406;
//static NSString *wsURL = @"http://pizzapi.herokuapp.com";
static NSString *wsURL = @"https://desolate-everglades-76324.herokuapp.com";

@interface CCMWSUtilities : NSObject

///Singleton...
+ (CCMWSUtilities *)sharedInstance;

//Exécute une requête de manière asynchrone...
+ (void)sendAsynchronousRequest:(NSMutableURLRequest *__strong *)request completion:(void (^)(NSInteger statusCode, id returnedJsonObject, NSError *error))completion;

//Envoie une requête asynchrone et indique la progression...
+ (void)sendAsynchronousRequest:(NSMutableURLRequest * __strong *)request progressBlock:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSInteger statusCode, id returnedJsonObject, NSError *error))completion;

//Ajoute un header ...
+ (void)addHeaderForRequest:(NSMutableURLRequest * __strong *)request;

@end
