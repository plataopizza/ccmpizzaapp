//
//  CCMWSUtilities+Pizzas.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMWSUtilities+Pizzas.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>

@implementation CCMWSUtilities (Pizzas)
+ (void)getAllPizzas:(void(^)(BOOL succeed, NSMutableArray<CCMPizza*>* pizzas, BOOL timedOut))completion{
    //Préparation de la requête...
    NSString *urlString = [[NSString stringWithFormat:@"%@/pizzas", wsURL] getFormattedURL];
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"GET"];
    
    [CCMWSUtilities sendAsynchronousRequest:&request completion:^(NSInteger statusCode, id returnedJsonObject, NSError *error) {
        if (statusCode == kLS_Succeed_Status_Code){
            if ([returnedJsonObject isKindOfClass:[NSArray class]]){
                RLMRealm *realm = [RLMRealm defaultRealm];
                
                NSMutableArray *array = [CCMPizza getArrayFromJsonObject:returnedJsonObject];
                [realm beginWriteTransaction];
                [realm deleteObjects:[CCMPizza allObjects]];
                for (CCMPizza *pizza in array) {
                    [realm addObject:pizza];
                }
                [realm commitWriteTransaction];
                if (completion) completion(YES, array, NO);
            }
            else {
                //Erreur...
                if (completion) completion(NO, nil, NO);
            }
        }
        else {
            //Erreur...
            if (statusCode == -1001) {
                if (completion) completion(NO, nil, YES);
                
            }else{
                if (completion) completion(NO, nil, NO);
                
            }
        }
        
    }];
}
@end
