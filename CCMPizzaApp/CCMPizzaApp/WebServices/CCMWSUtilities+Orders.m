//
//  CCMWSUtilities+Orders.m
//  CCMPizzaApp
//
//  Created by Valentin Denis on 18/10/2016.
//  Copyright © 2016 Guival. All rights reserved.
//

#import "CCMWSUtilities+Orders.h"
#import <Phoceis_SDK_iOS/Phoceis_SDK_iOS.h>


@implementation CCMWSUtilities (Orders)

+ (void)getAllOrders:(void(^)(BOOL succeed, NSMutableArray<CCMOrder*>*orders, BOOL timedOut))completion{
    //Préparation de la requête...
    NSString *urlString = [[NSString stringWithFormat:@"%@/orders", wsURL] getFormattedURL];
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"GET"];
    
    [CCMWSUtilities sendAsynchronousRequest:&request completion:^(NSInteger statusCode, id returnedJsonObject, NSError *error) {
        if (statusCode == kLS_Succeed_Status_Code){
            if ([returnedJsonObject isKindOfClass:[NSArray class]]){
                RLMRealm *realm = [RLMRealm defaultRealm];
                
                NSMutableArray *orders = [CCMOrder getArrayFromJsonObject:returnedJsonObject];
                [realm beginWriteTransaction];
                [realm deleteObjects:[CCMOrder allObjects]];
                for (CCMOrder *order in orders) {
                    [realm addObject:order];
                }
                [realm commitWriteTransaction];
                
                if (completion) completion(YES, orders, NO);
            }
            else {
                //Erreur...
                if (completion) completion(NO, nil, NO);
            }
        }
        else {
            //Erreur...
            if (statusCode == -1001) {
                if (completion) completion(NO, nil, YES);

            }else{
                if (completion) completion(NO, nil, NO);

            }
        }
        
    }];

}

+ (void)getOrderWithID:(NSString *)identifier completion:(void(^)(BOOL succeed, CCMOrder *order, BOOL timedOut))completion{
    //Préparation de la requête...
    NSString *urlString = [[NSString stringWithFormat:@"%@/orders/%@", wsURL, identifier] getFormattedURL];
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"GET"];
    
    [CCMWSUtilities sendAsynchronousRequest:&request completion:^(NSInteger statusCode, id returnedJsonObject, NSError *error) {
        if (statusCode == kLS_Succeed_Status_Code){
            if ([returnedJsonObject isKindOfClass:[NSDictionary class]]){
                CCMOrder *order = [[CCMOrder alloc] initWithJsonObject:returnedJsonObject];
                if (completion) completion(YES, order, NO);
            }
            else {
                //Erreur...
                if (completion) completion(NO, nil, NO);
            }
        }
        else {
            //Erreur...
            if (statusCode == -1001) {
                if (completion) completion(NO, nil, YES);
                
            }else{
                if (completion) completion(NO, nil, NO);
                
            }
        }
        
    }];
}

+ (void)postOrder:(CCMPizza *)pizza completion:(void(^)(BOOL succeed, CCMOrder *order, BOOL timedOut))completion{
    //Préparation de la requête...
    NSString *urlString = [[NSString stringWithFormat:@"%@/orders", wsURL] getFormattedURL];
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    NSDictionary *dict = @{@"id":[NSNumber numberWithInteger:pizza.pizzaID]};
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    [request setHTTPBody:data];
    
    [CCMWSUtilities sendAsynchronousRequest:&request completion:^(NSInteger statusCode, id returnedJsonObject, NSError *error) {
        if (statusCode == kLS_Succeed_Status_Code){
            if ([returnedJsonObject isKindOfClass:[NSDictionary class]]){
                CCMOrder *order = [[CCMOrder alloc] initWithJsonObject:returnedJsonObject];
                if (completion) completion(YES, order, NO);
            }
            else {
                //Erreur...
                if (completion) completion(NO, nil, NO);
            }
        }
        else {
            //Erreur...
            if (statusCode == -1001) {
                if (completion) completion(NO, nil, YES);
                
            }else{
                if (completion) completion(NO, nil, NO);
                
            }
        }
        
    }];
}

@end
